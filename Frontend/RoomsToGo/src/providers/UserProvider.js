import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {createContext, useContext, useEffect, useState} from 'react';
import {Alert} from 'react-native';

const UserContext = createContext(null);

export const UserProvider = ({children}) => {
  const [user, setUser] = useState(null);
  const [cart, setCart] = useState(null);

  const getUser = async () => {
    const stringifiedUser = await AsyncStorage.getItem('@storage_Key');
    const user = JSON.parse(stringifiedUser);
    setUser(user);
  };

  const getCart = async () => {
    try {
      const {data} = await axios.get(`/getCart`, {
        params: {
          userId: user,
        },
      });

      setCart(data);
    } catch (err) {
      Alert(err.message);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  useEffect(() => {
    if (user) {
      getCart();
    }
  }, [user]);

  return (
    <UserContext.Provider value={{user, setUser, cart, setCart}}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);
