import React, {useState, useEffect, useCallback} from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Modal} from 'react-native';
import axios from 'axios';
import ProductGrid from '../Components/ProductGrid';
import {useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DropDownPicker from 'react-native-dropdown-picker';
import Button from '../Components/Button';

const ProductsScreen = ({route, navigation}) => {
  const {productType} = route.params;

  const [cartId, setCartId] = React.useState('');
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [modalVisibleFilter, setModalVisibleFilter] = useState(false);
  const [modalVisibleSort, setModalVisibleSort] = useState(false);

  const [price, setPrice] = useState();
  const [priceOpen, setPriceOpen] = useState(false);
  const [priceOptions, setPriceOptions] = useState([
    {label: '100', value: '100'},
    {label: '250', value: '250'},
  ]);

  const [color, setColor] = useState();
  const [colorOpen, setColorOpen] = useState(false);
  const [colorOptions, setColorOptions] = useState([
    {label: 'white', value: 'white'},
    {label: 'black', value: 'black'},
  ]);

  const [materials, setMaterials] = useState();
  const [materialsOpen, setMaterialsOpen] = useState(false);
  const [materialsOptions, setMaterialsOptions] = useState([
    {label: 'wood', value: 'wood'},
    {label: 'Particleboard', value: 'particleboard'},
  ]);

  const [sortByPrice, setSortByPrice] = useState();
  const [sortOrderPrice, setSortOrderPrice] = useState();
  const [sortOrderPrOpen, setSortOrderPrOpen] = useState(false);
  const [sortOrderPrOptions, setSortOrderPrOptions] = useState([
    {label: 'Price:low to high', value: 'asc'},
    {label: 'Price:high to low', value: 'desc'},
  ]);
  const onSortPriceOpen = useCallback(() => {
    setSortByPrice('price');
  }, []);

  const [sortByName, setSortByName] = useState();
  const [sortOrderName, setSortOrderName] = useState();
  const [sortOrderNmOpen, setSortOrderNmOpen] = useState(false);
  const [sortOrderNmOptions, setSortOrderNmOptions] = useState([
    {label: 'From A to Z', value: 'asc'},
    {label: 'To Z to A', value: 'desc'},
  ]);
  const onSortNameOpen = useCallback(() => {
    setSortByName('name');
    setSortOrderPrOpen(false);
  }, []);

  const onPriceOpen = useCallback(() => {
    setMaterialsOpen(false);
    setColorOpen(false);
  }, []);

  const onColorOpen = useCallback(() => {
    setMaterialsOpen(false);
    setPriceOpen(false);
  }, []);

  const onMaterialOpen = useCallback(() => {
    setPriceOpen(false);
    setColorOpen(false);
  }, []);

  const handleChangePage = () => {
    if (page !== 1) {
      setPage(page - 1);
    }
  };

  const isFocused = useIsFocused();

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      console.log(data);
      setCartId(data.cartId);
    } catch (e) {}
  };

  const fetchProducts = async () => {
    try {
      const res = await axios
        .get('/getAllProducts', {
          params: {
            productType: productType,
            pageNumber: page > 0 ? page : 1,
            nrOfProducts: 6,
          },
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setProducts(res?.data);
      }
    } catch {
      console.log('error fetching the products');
    }
  };

  const getFilteredProducts = async () => {
    try {
      const res = await axios
        .get('/getFilteredProducts', {
          params: {
            price: price,
            pageNumber: page > 0 ? page : 1,
            nrOfProducts: 6,
            productType: productType,
          },
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setProducts(res?.data);
      }
    } catch {
      console.log('error fetching the products');
    }
  };

  const getSortedProducts = async () => {
    try {
      const res = await axios
        .get('/getSortedProducts', {
          params: {
            sortBy: sortByPrice || sortByName,
            sortOrder: sortOrderPrice || sortOrderName,
            productType: productType,
            pageNumber: page > 0 ? page : 1,
            nrOfProducts: 6,
          },
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setProducts(res?.data);
      }
    } catch {
      console.log('error fetching the products');
    }
  };

  useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  useEffect(() => {
    fetchProducts();
    getSortedProducts();
  }, [page, sortByName, sortByPrice]);

  return (
    <View
      style={[
        styles.productsContainer,
        modalVisibleFilter ? {backgroundColor: 'rgba(0,0,0,0.5)'} : '',
      ]}>
      <Modal
        visible={modalVisibleFilter}
        style={{height: 10}}>
        <View style={styles.testView}>
          <View style={styles.modalView}>
            <View style={{marginTop: 10, marginBottom: 10}}>
              <DropDownPicker
                placeholder="Select a Price"
                open={priceOpen}
                value={price}
                items={priceOptions}
                onOpen={onPriceOpen}
                setOpen={setPriceOpen}
                setValue={setPrice}
                setItems={setPriceOptions}
                zIndex={3000}
                zIndexInverse={1000}
                dropDownDirection="TOP"
              />
            </View>
            <View style={{marginTop: 10, marginBottom: 10}}>
              <DropDownPicker
                placeholder="Select a Color"
                open={colorOpen}
                value={color}
                items={colorOptions}
                onOpen={onColorOpen}
                setOpen={setColorOpen}
                setValue={setColor}
                setItems={setColorOptions}
                zIndex={2000}
                zIndexInverse={2000}
                dropDownDirection="TOP"
              />
            </View>
            <View style={{marginTop: 10, marginBottom: 10}}>
              <DropDownPicker
                placeholder="Select a Material"
                open={materialsOpen}
                value={materials}
                items={materialsOptions}
                onOpen={onMaterialOpen}
                setOpen={setMaterialsOpen}
                setValue={setMaterials}
                setItems={setMaterialsOptions}
                zIndex={1000}
                zIndexInverse={3000}
                dropDownDirection="TOP"
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                getFilteredProducts();
                setModalVisibleFilter(false);
              }}
              style={{marginTop: 20}}>
              <Text>Close modal</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <Modal visible={modalVisibleSort} style={{height: 10}}>
        <View style={styles.testView}>
          <View style={styles.modalView}>
            <Text style={styles.sortModalTitle}>Sort By:</Text>
            <View style={styles.pickerWrapper}>
              <DropDownPicker
                placeholder="Price"
                open={sortOrderPrOpen}
                value={sortOrderPrice}
                items={sortOrderPrOptions}
                onOpen={onSortPriceOpen}
                setOpen={setSortOrderPrOpen}
                setValue={setSortOrderPrice}
                setItems={setSortOrderPrOptions}
                zIndex={3000}
                zIndexInverse={1000}
                dropDownDirection="TOP"
                disabled={true}
              />
            </View>
            <View style={styles.pickerWrapper}>
              <DropDownPicker
                placeholder="Name"
                open={sortOrderNmOpen}
                value={sortOrderName}
                items={sortOrderNmOptions}
                onOpen={onSortNameOpen}
                setOpen={setSortOrderNmOpen}
                setValue={setSortOrderName}
                setItems={setSortOrderNmOptions}
                zIndex={2000}
                zIndexInverse={2000}
                dropDownDirection="TOP"
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                getSortedProducts();
                setModalVisibleSort(false);
              }}
              style={{marginTop: 20}}>
              <Text>Close modal</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <View style={styles.filtersContainer}>
        <Button
          onPress={() => setModalVisibleFilter(true)}
          customStyle={styles.filterButton}
          labelStyle={styles.filteringText}
          label={'Filter'}
        />
        <Button
          onPress={() => setModalVisibleSort(true)}
          customStyle={styles.filterButton}
          labelStyle={styles.filteringText}
          label={'Sort'}
        />
      </View>
      <ProductGrid
        products={products}
        cartId={cartId}
        showAddToCart={true}
        navigation={navigation}
      />
      <View style={styles.buttonsContainer}>
        {page != 1 && (
          <TouchableOpacity
            style={styles.buttonPrev}
            onPress={() => handleChangePage()}>
            <Text style={styles.buttonText}>Prev Page</Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={styles.buttonNext}
          onPress={() => setPage(page + 1)}>
          <Text style={styles.buttonText}>Next Page</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  productsContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 45,
    backgroundColor: 'rgba(255, 255, 255, 0.0)',
    borderRadius: 5,
  },
  buttonPrev: {
    backgroundColor: '#284B63',
    marginTop: 5,
    marginBottom: 5,
    width: 120,
    borderRadius: 15,
    marginLeft: 10,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  buttonNext: {
    backgroundColor: '#284B63',
    marginTop: 5,
    marginBottom: 5,
    width: 120,
    borderRadius: 15,
    marginRight: 10,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    position: 'absolute',
    right: 1,
  },
  buttonText: {
    alignItems: 'center',
    fontSize: 15,
    color: 'white',
  },
  testView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 0,
    marginBottom: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.3)',
    position: 'absolute',
    borderRadius: 10,
  },
  modalView: {
    marginTop: 20,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  filterButton: {
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 10,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    overflow: 'hidden',
    marginHorizontal: 1,
    width: 100,
  },
  sortModalTitle: {
    marginBottom: 15,
    fontSize: 15,
  },
  filtersContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  filteringText: {
    justifyContent: 'center',
    color: 'black',
    fontSize: 15,
  },
  pickerWrapper: {
    marginTop: 10,
    marginBottom: 10,
  },
});

export default ProductsScreen;
