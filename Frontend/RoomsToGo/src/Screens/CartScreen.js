import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CartGrid from '../Components/CartGrid';
import axios from 'axios';

const CartScreen = props => {
  const {navigation} = props;
  const isFocused = useIsFocused();
  const [changedQuantity, setChangedQuantity] = React.useState(false);
  const [products, setProducts] = React.useState([]);
  const [total, setTotal] = React.useState('');
  const [cartId, setCartId] = React.useState();

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data !== null) {
        setCartId(data.cartId);
        fetchProducts(data.cartId);
        fetchTotalSum(data.cartId);
      }
    } catch (e) {}
  };

  const onChangeQuantity = q => {
    if (q < 1) {
      fetchProducts(cartId);
    }
    setChangedQuantity(!changedQuantity);
  };

  const createProductArray = data => {
    let productArray = [];
    let i;
    for (i = 0; i < data.length; i++) {
      let productAux = data[i].__product__;
      productAux.quantity = data[i].quantity;
      productArray.push(productAux);
    }
    return productArray;
  };

  const fetchProducts = async cartId => {
    try {
      const res = await axios
        .post('/showProductsInCart', {
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        setProducts(createProductArray(res.data));
      }
    } catch {
      console.log('error fetching the products');
    }
  };

  const fetchTotalSum = async cartId => {
    try {
      const res = await axios
        .post('/calculateCartTotalSum', {
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        setTotal(res.data);
      }
    } catch {
      console.log('error fetching the products');
    }
  };

  React.useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  React.useEffect(() => {
    fetchTotalSum(cartId);
  }, [changedQuantity]);
  return (
    <View style={styles.container}>
      <CartGrid
        products={products}
        cartId={cartId}
        onChangeQuantity={onChangeQuantity}
      />
      <View style={{alignItems: 'center'}}>
        <View style={styles.totalPrice}>
          <Text style={styles.price}>{total}</Text>
        </View>
        <TouchableOpacity
          style={styles.checkOutButton}
          onPress={() =>
            navigation.navigate('CheckOut', {
              total: total,
            })
          }>
          <Text style={styles.checkOutText}>Checkout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  titleContainer: {
    flexDirection: 'row',
    marginBottom: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
  cartIcon: {
    marginLeft: 8,
  },
  cartTitle: {
    fontSize: 18,
    color: '#808080',
    marginLeft: 8,
    marginTop: 4,
  },
  totalPrice: {
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 30,
    paddingTop: 15,
    paddingBottom: 15,
    width: 200,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: 'center',
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#284B63',
  },
  checkOutButton: {
    backgroundColor: '#284B63',
    marginTop: 20,
    marginBottom: 15,
    width: 230,
    borderRadius: 15,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  checkOutText: {
    alignItems: 'center',
    fontSize: 18,
    color: 'white',
  },
});

export default CartScreen;
