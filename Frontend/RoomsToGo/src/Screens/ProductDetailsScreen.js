import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Modal,
  Alert,
} from 'react-native';
import axios from 'axios';
import Button from '../Components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ReviewModal from '../Components/ReviewModal';

const ProductDetailsScreen = props => {
  const {navigation} = props;
  const [reviews, setReviews] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [reviewModal, setReviewModal] = useState(false);
  const [userId, setUserId] = useState('');
  const [reviewEligible, setReviewEligible] = useState([]);
  const [firstName, setFirstName] = useState('');
  const [cartId, setCartId] = useState('');

  const productId = props.route.params.productId;

  const getReviews = async () => {
    try {
      const res = await axios
        .get('/getReviews', {
          params: {
            productId: productId,
          },
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setReviews(res?.data);
      }
    } catch {
      console.log('error fetching the reviews');
    }
  };

  const getReviewEligibility = async id => {
    try {
      const res = await axios
        .get('/checkReviewEligibility', {
          params: {
            userId: id,
            productId: productId,
          },
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setReviewEligible(res.data);
      }
    } catch {
      console.log('error fetching the reviews');
    }
  };

  const addToCart = async () => {
    try {
      const res = await axios
        .post('/addToCart', {
          productId,
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
    } catch {
      console.log('Cannot add to cart');
    }
  };

  const toggleModal = () => setModalVisible(!modalVisible);

  const toggleModalReview = () => setReviewModal(!reviewModal);

  const handleGiveReview = () => {
    if (reviewEligible?.code == 3) {
      Alert.alert(reviewEligible.message);
    } else {
      setReviewModal(!reviewModal);
    }
  };

  const avgRating =
    reviews.reduce((acc, curr) => acc + curr.rating, 0) / reviews.length;

  const renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text style={styles.modalTitle}>Reviews</Text>
      <View style={styles.ratingContainer}>
        <Text style={styles.avgRating}>
          {avgRating > 0 ? avgRating.toFixed(1) : ''}
        </Text>
        <Text style={styles.ratingStars}>★★★★★</Text>
      </View>
      {reviews && reviews.length > 0 ? (
        reviews.map(review => (
          <View key={review.id} style={styles.reviewItem}>
            <View style={{paddingLeft: 5}}>
              <Text style={styles.reviewRating}>{review.firstName}</Text>
            </View>
            <View style={{paddingLeft: 5}}>
              <Text style={styles.reviewRating}>{review.rating} stars</Text>
            </View>
            <View style={{paddingLeft: 5}}>
              <Text style={styles.reviewComment}>{review.comment}</Text>
            </View>
          </View>
        ))
      ) : (
        <View>
          <Text>No reviews for this product!</Text>
        </View>
      )}
      <View style={styles.buttonsContainer}>
        <Button
          label={'Add Review'}
          customStyle={{marginTop: 20}}
          onPress={handleGiveReview}
        />
        <Button
          onPress={toggleModal}
          label={'Close'}
          customStyle={{marginTop: 10}}
        />
        <ReviewModal
          visible={reviewModal}
          onClose={toggleModalReview}
          productId={productId}
          userId={userId}
          userName={firstName}
        />
      </View>
    </View>
  );

  useEffect(() => {
    getReviews();
  }, []);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data) {
        getReviewEligibility(data.id);
        setUserId(data.id);
        setCartId(data.cartId);
        setFirstName(data.firstName);
      }
    } catch (e) {}
  };

  useEffect(() => {
    getData();
  }, []);


  return (
    <ScrollView style={styles.cardContainer}>
      <View style={styles.imageContainer}>
        <Image
          source={{uri: props.route.params.image}}
          style={{height: 240, width: 240}}
        />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{props.route.params.productName}</Text>
      </View>
      <TouchableOpacity style={styles.reviewContainer} onPress={toggleModal}>
        <Text>Reviews</Text>
      </TouchableOpacity>
      <Modal visible={modalVisible} transparent={true}>
        {renderModalContent()}
      </Modal>
      <View style={styles.reviewContainer}>
        <Text>Color: Pink</Text>
      </View>
      <View style={styles.reviewContainer}>
        <Text>Description</Text>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity style={styles.cartButton} onPress={() => addToCart()}>
          <Text style={styles.cartText}>Add to Cart</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: 'white',
    flex: 1,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    padding: 5,
  },
  titleContainer: {
    paddingHorizontal: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
  },
  reviewContainer: {
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  cartButton: {
    backgroundColor: '#9EB7B8',
    marginTop: 8,
    marginBottom: 5,
    width: 250,
    borderRadius: 8,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartText: {
    alignItems: 'center',
    fontSize: 14,
    color: 'white',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    flex: 1,
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  avgRating: {
    fontSize: 32,
    marginRight: 10,
    fontWeight: 'bold',
  },
  ratingStars: {
    fontSize: 18,
    color: 'grey',
  },
  reviewItem: {
    margin: 4,
    borderWidth: 2,
    borderRadius: 5,
    width: '105%',
    borderColor: '#d9d9d9',
  },
  reviewRating: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  reviewComment: {
    fontSize: 16,
  },
  closeButton: {
    marginTop: 20,
    color: '#9EB7B8',
  },
  buttonsContainer: {
    justifyContent: 'space-between',
    flex: 0.5,
  },
});

export default ProductDetailsScreen;
