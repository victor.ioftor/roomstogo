import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const CheckOutScreen = ({navigation, route}) => {
  const [address, setAddress] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();
  const [userId, setUserId] = useState('');
  const isFocused = useIsFocused();

  const {total} = route.params;

  const totalSum = total;

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data) {
        fetchUserDetails(data.id);
        setUserId(data.id);
      }
    } catch (e) {}
  };

  const fetchUserDetails = async userId => {
    try {
      const res = await axios
        .post('/getUserDetails', {
          userId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        setFirstName(res.data.firstName);
        setLastName(res.data.lastName);
        setPhoneNumber(res.data.phoneNumber);
        setAddress(res.data.address);
      }
    } catch {
      console.log('error fetching the user details');
    }
  };

  const createOrder = async () => {
    try {
      const res = await axios
        .post('/createOrder', {
          firstName,
          lastName,
          totalSum,
          address,
          phoneNumber,
          userId,
        })
        .catch(function (error) {
          if (error.response) {
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        alert('Order created succesfully');
        navigation.navigate('Payment');
      }
    } catch {
      console.log('cannot create order');
    }
  };

  useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  return (
    <ScrollView style={styles.scrollStyle}>
      <View style={styles.header}>
        <Text style={styles.headerText}>CheckOut</Text>
      </View>
      <View style={styles.container}>
        <View style={styles.inputView}>
          <TextInput
            value={firstName}
            placeholder="First Name"
            style={styles.textInput}
            editable={false}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={lastName}
            placeholder="Last Name"
            style={styles.textInput}
            editable={false}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={address}
            placeholder="Address"
            style={styles.textInput}
            onChangeText={newText => setAddress(newText)}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={phoneNumber}
            placeholder="Phone Number"
            style={styles.textInput}
            onChangeText={newText => setPhoneNumber(newText)}
          />
        </View>
        <View style={styles.totalPrice}>
          <Text style={styles.price}>{totalSum}</Text>
        </View>
        <View>
          <TouchableOpacity
            style={styles.paymentButton}
            onPress={() => {
              createOrder();
            }}>
            <Text style={styles.paymentText}>Continue to Payment</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollStyle: {
    backgroundColor: '#fff',
  },
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  headerText: {
    fontSize: 18,
    color: '#808080',
  },
  inputView: {
    marginBottom: 25,
    marginTop: 20,
    alignItems: 'center',
    marginBottom: 10,
  },
  textInput: {
    backgroundColor: '#D9D9D9',
    height: 70,
    width: 350,
    borderRadius: 15,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  paymentButton: {
    backgroundColor: '#284B63',
    marginTop: 30,
    width: 250,
    borderRadius: 15,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  paymentText: {
    alignItems: 'center',
    fontSize: 15,
    color: 'white',
  },
  registerContainer: {
    position: 'absolute',
    top: 190,
    left: -85,
  },
  registerTextContainer: {
    fontSize: 16,
    alignItems: 'center',
    fontWeight: '300',
  },
  totalPrice: {
    borderColor: '#000000',
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 30,
    paddingTop: 15,
    paddingBottom: 15,
    width: 200,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: 'center',
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#284B63',
  },
});
export default CheckOutScreen;
