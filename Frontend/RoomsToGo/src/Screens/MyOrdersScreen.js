import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OrdersGrid from '../Components/OrdersGrid';

const MyOrdersScreen = () => {
  const [id, setId] = useState('');
  const isFocused = useIsFocused();
  const [orders, SetOrders] = useState([]);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data) {
        console.log('data ID', data.id);
        fetchOrders(data.id);
        setId(data.id);
      }
    } catch (e) {}
  };

  const fetchOrders = async id => {
    try {
      const res = await axios
        .post('/getOrders', {
          id,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        SetOrders(res?.data);
      }
    } catch {
      console.log('error fetching the user details');
    }
  };

  useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  return (
    <>
      <View style={styles.textContainer}>
        <Text style={{fontSize: 20}}>Here you can see all the orders</Text>
      </View>
      <View></View>
      <OrdersGrid orders={orders} />
    </>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    marginTop: 50,
    alignItems: 'center',
    marginBottom: 50,
  },
});

export default MyOrdersScreen;
