import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNRestart from 'react-native-restart';
import {useIsFocused} from '@react-navigation/native';

const AccountDetailsScreen = props => {
  const {navigation} = props;
  const [id, setId] = useState('');
  const isFocused = useIsFocused();
  const [firstName, setFirstName] = useState('');

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data) {
        setFirstName(data.firstName);
        fetchUserDetails(data.id);
        setId(data.id);
      }
    } catch (e) {}
  };

  const clearStorage = async () => {
    try {
      await AsyncStorage.clear();
      alert('Succesfully logged out!!');
      RNRestart.Restart();
    } catch (e) {
      alert('Failed to log out');
    }
  };

  useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image
          source={require('../assets/imagines/logo.png')}
          style={styles.logo}
        />
      </View>
      <View style={styles.header}>
        <Text style={styles.headerText}>Welcome back, {firstName}!</Text>
      </View>
      <View style={{justifyContent: 'center', marginTop: 300}}>
        <TouchableOpacity
          style={styles.editAccountButton}
          onPress={() => navigation.navigate('Edit Account')}>
          <Text style={styles.buttonText}>Edit Account</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.ordersButton}
          onPress={() => navigation.navigate('Orders')}>
          <Text style={styles.buttonText}>Orders</Text>
        </TouchableOpacity>
      </View>
      <View>
        <TouchableOpacity
          style={styles.signOutButton}
          onPress={() => clearStorage()}>
          <Text style={styles.buttonText}>Sign out</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  logoContainer: {
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    right: 130,
    backgroundColor: 'white',
  },
  logo: {
    resizeMode: 'cover',
    height: 160,
    width: 150,
  },
  editAccountButton: {
    backgroundColor: '#9EB7B8',
    position: 'absolute',
    bottom: 5,
    right: -150,
    width: 300,
    borderRadius: 15,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ordersButton: {
    backgroundColor: '#9EB7B8',
    position: 'absolute',
    top: 30,
    right: -150,
    width: 300,
    borderRadius: 15,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  signOutButton: {
    backgroundColor: '#9EB7B8',
    position: 'absolute',
    top: 260,
    right: -150,
    width: 300,
    borderRadius: 15,
    height: 65,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    alignItems: 'center',
    fontSize: 15,
    color: 'white',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    top: 150,
    marginTop: 10,
  },
  headerText: {
    fontSize: 18,
    color: '#808080',
  },
});

export default AccountDetailsScreen;
