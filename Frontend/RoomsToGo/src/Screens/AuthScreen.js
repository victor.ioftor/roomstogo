import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  Alert,
} from 'react-native';
import Button from '../Components/Button';
import auth from '@react-native-firebase/auth';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-google-signin/google-signin';
import {useUser} from '../providers/UserProvider';

GoogleSignin.configure({
  webClientId:
    '41150879410-mokqnmrhav5ui5dm8cij0sesue4udqv1.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
});

const AuthScreen = props => {
  const {navigation} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const [userInfo, setUserInfo] = useState();

  const {setUser} = useUser();

  const saveData = async data => {
    try {
      const jsonValue = JSON.stringify(data);
      await AsyncStorage.setItem('@storage_Key', jsonValue);
      setUser(data);
    } catch (e) {}
  };
  const handleAuth = async () => {
    if (email.length != 0 && password.length != 0) {
      try {
        const res = await axios
          .post('/loginUser', {
            email,
            password,
          })
          .catch(function (error) {
            if (error.response) {
              setAlertMessage(error.response.data.text);
            } else if (error.request) {
              console.log(error.request);
            } else {
              console.log('Error', error.message);
            }
          });
        if (res.data) {
          saveData(res.data);
          navigation.navigate('My Account');
        }
      } catch {
        console.log('Login not ok');
      }
    } else {
      setAlertMessage('Please fill in all the fields!');
    }
  };

  const signIn = async () => {
    const {idToken} = await GoogleSignin.signIn().catch(e => {
      Alert.alert(e.message);
      setLoading(false);
    });

    const googleCredential = await auth.GoogleAuthProvider.credential(idToken);

    await auth()
      .signInWithCredential(googleCredential)
      .then(async res => {
        setUserInfo(res.additionalUserInfo.profile);
        const response = await axios.post('/registerGoogleUser', {
          firstName: res.additionalUserInfo.profile.given_name,
          lastName: res.additionalUserInfo.profile.family_name,
          email: res.additionalUserInfo.profile.email,
          isGoogleUser: true,
        });
        saveData(response.data);
        navigation.navigate('My Account');
      })
      .catch(e => {
        Alert.alert(e.message);
      });
    const accessToken = await (await GoogleSignin.getTokens()).accessToken;
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image
          source={require('../assets/imagines/logo.png')}
          style={styles.logo}
        />
      </View>
      <View>
        <Text style={styles.errorAlert}>{alertMessage}</Text>
      </View>
      <View style={styles.inputView}>
        <TextInput
          value={email}
          placeholder="Email"
          style={styles.textInput}
          onChangeText={newText => setEmail(newText)}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          value={password}
          placeholder="Password"
          style={styles.textInput}
          secureTextEntry
          onChangeText={newText => setPassword(newText)}
        />
      </View>
      <View>
        <Button
          customStyle={styles.logInButton}
          onPress={() => {
            handleAuth();
          }}
          label={'Log In'}
          width={150}
        />
      </View>
      <GoogleSigninButton
        style={{width: 180, height: 48, padding: 20, marginTop: 20}}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Light}
        onPress={() => signIn()}
      />
      <View>
        <Button
          customStyle={styles.registerContainer}
          label={' No account? Sign up here'}
          onPress={() => navigation.navigate('Register')}
          width={250}
          height={30}
          backgroundColor={'#82A2A3'}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContainer: {
    position: 'absolute',
    top: -40,
    alignItems: 'center',
    right: 100,
  },
  logo: {
    resizeMode: 'contain',
    height: 300,
    width: 200,
  },
  inputView: {
    marginBottom: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  textInput: {
    backgroundColor: '#E4E4E4',
    height: 70,
    width: 360,
    borderRadius: 10,
    alignItems: 'center',
    paddingLeft: 20,
  },
  logInButton: {
    marginTop: 20,
  },
  registerContainer: {
    position: 'absolute',
    top: 130,
    left: -115,
    borderRadius: 10,
  },
  registerTextContainer: {
    fontSize: 16,
    alignItems: 'center',
    fontWeight: '300',
  },
  passwordIcon: {
    position: 'absolute',
    top: 3,
    right: 327,
    padding: 10,
  },
  emailIcon: {
    position: 'absolute',
    top: 3,
    right: 330,
    padding: 10,
  },
});

export default AuthScreen;
