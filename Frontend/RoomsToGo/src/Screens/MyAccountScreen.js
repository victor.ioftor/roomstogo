import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from '../Components/Button';

const MyAccountScreen = props => {
  const {navigation} = props;

  const [email, setEmail] = useState('');
  const [lastName, setLastName] = useState('');
  const [firstName, setFirstName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [address, setAddress] = useState('');
  const [id, setId] = useState('');
  const isFocused = useIsFocused();

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      const data = JSON.parse(jsonValue);
      if (data) {
        fetchUserDetails(data.id);
        setId(data.id);
      }
    } catch (e) {}
  };

  const saveUserDetails = async () => {
    try {
      const res = await axios
        .post('/updateUserDetails', {
          id,
          firstName,
          lastName,
          phoneNumber,
          address,
        })
        .catch(function (error) {
          if (error.response) {
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        alert('Details saved succesfully');
        navigation.navigate('My Account');
      }
    } catch {
      console.log('cannot update details');
    }
  };

  const fetchUserDetails = async id => {
    try {
      const res = await axios
        .post('/getUserDetails', {
          userId: id,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res.data) {
        setEmail(res.data.email);
        setFirstName(res.data.firstName);
        setLastName(res.data.lastName);
        setPhoneNumber(res.data.phoneNumber);
        setAddress(res.data.address);
      }
    } catch {
      console.log('error fetching the user details');
    }
  };

  React.useEffect(() => {
    if (isFocused) getData();
  }, [isFocused]);

  return (
    <ScrollView style={{backgroundColor: '#fff'}}>
      <View style={styles.container}>
        <View style={styles.inputView}>
          <TextInput
            value={firstName}
            placeholder="First Name"
            style={styles.textInput}
            autoCapitalize="words"
            onChangeText={newText => setFirstName(newText)}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={lastName}
            placeholder="Last Name"
            style={styles.textInput}
            autoCapitalize="words"
            onChangeText={newText => setLastName(newText)}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            editable={false}
            value={email}
            placeholder="Change Email"
            style={styles.textInput}
            keyboardTypes="email"
            autoCapitalize="none"
            onChangeText={newText => setEmail(newText)}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={phoneNumber}
            placeholder="Phone Number"
            style={styles.textInput}
            keyboardType="numeric"
            onChangeText={newText => setPhoneNumber(newText)}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            value={address}
            placeholder="Address"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={newText => setAddress(newText)}
          />
        </View>
        <View>
          <Button
            customStyle={styles.editAccountButton}
            onPress={() => saveUserDetails()}
            label={'Edit Account'}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputView: {
    marginBottom: 25,
    marginTop: 20,
    alignItems: 'center',
    marginBottom: 10,
  },
  textInput: {
    backgroundColor: '#D9D9D9',
    height: 70,
    width: 350,
    borderRadius: 15,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  editAccountButton: {
    backgroundColor: '#9EB7B8',
    position: 'absolute',
    top: 60,
    left: -70,
    width: 150,
    borderRadius: 15,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    alignItems: 'center',
    fontSize: 15,
    color: 'white',
  },
});

export default MyAccountScreen;
