import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import ProductGrid from '../Components/ProductGrid';
import Button from '../Components/Button';

const HomePageScreen = props => {
  const {navigation} = props;
  const [data, setData] = useState();
  const [products, setProducts] = useState();

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      setData(jsonValue);
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      alert('Failed to save the data to the storage');
    }
  };

  const fetchRandomProducts = async () => {
    try {
      const res = await axios
        .get('/getRandomProducts', {})
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
      if (res?.data) {
        setProducts(res?.data);
      }
    } catch {
      console.log('error at fetching random products');
    }
  };

  useEffect(() => {
    getData();
    fetchRandomProducts();
  }, []);

  return (
    <>
      <View style={styles.welcomeContainer}>
        <Text style={styles.welcomeTitle}>Welcome !</Text>
      </View>
      <ScrollView
        horizontal={true}
        contentContainerStyle={styles.productSwiper}
        style={styles.container}>
        <TouchableOpacity
          style={styles.productsIcon}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: 'table',
            })
          }>
          <MaterialCommunityIcons
            name="table-furniture"
            size={40}
            color="black"
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.productsIcon}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: 'chair',
            })
          }>
          <MaterialCommunityIcons
            name="chair-rolling"
            size={40}
            color="black"
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.productsIcon}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: 'wardrobe',
            })
          }>
          <MaterialCommunityIcons name="wardrobe" size={40} color="black" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.productsIcon}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: 'sofa',
            })
          }>
          <MaterialCommunityIcons name="sofa" size={40} color="black" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.productsIcon}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: 'bed-king',
            })
          }>
          <MaterialCommunityIcons name="bed-king" size={40} color="black" />
        </TouchableOpacity>
      </ScrollView>
      <ProductGrid
        products={products}
        showAddToCart={false}
        navigation={navigation}
      />
      <View style={styles.buttonContainer}>
        <Button
          label={'Products'}
          onPress={() =>
            navigation.navigate('ProductsPage', {
              productType: '',
            })
          }
          backgroundColor={'#9EB7B8'}
          width={200}
          height={50}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    flexGrow: 0,
    paddingBottom: 10,
    backgroundColor: 'white',
  },
  welcomeContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcomeTitle: {
    fontSize: 15,
    color: 'black',
    fontWeight: '500',
  },
  productSwiper: {
    justifyContent: 'space-evenly',
    flex: 1,
    backgroundColor: 'white',
  },
  productsIcon: {
    padding: 5,
  },
  buttonContainer: {
    flex: 0.5,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartButton: {
    backgroundColor: '#9EB7B8',
    marginTop: 50,
    marginBottom: 5,
    width: 200,
    borderRadius: 15,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartText: {
    alignItems: 'center',
    fontSize: 17,
    color: 'white',
  },
});

export default HomePageScreen;
