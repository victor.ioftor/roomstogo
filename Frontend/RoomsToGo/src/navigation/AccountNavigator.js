import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Header from '../Components/Header';
import MyAccountScreen from '../Screens/MyAccountScreen';
import AccountDetailsScreen from '../Screens/AccountDetailsScreen';
import MyOrdersScreen from '../Screens/MyOrdersScreen';
const Stack = createStackNavigator();

export const AccountNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: ({navigation, options}) => {
          return <Header navigation={navigation} title={options?.title} />;
        },
      }}>
      <Stack.Screen
        name="My Account"
        component={AccountDetailsScreen}
        options={{title: 'My Account'}}
      />
      <Stack.Screen
        name="Edit Account"
        component={MyAccountScreen}
        options={{title: 'Edit Details'}}
      />
      <Stack.Screen
        name="Orders"
        component={MyOrdersScreen}
        options={{title: 'My Orders'}}
      />
    </Stack.Navigator>
  );
};
