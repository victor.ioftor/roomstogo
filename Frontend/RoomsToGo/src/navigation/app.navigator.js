import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {HomePageNavigator} from './HomePageNavigator';
import {CartScreenNavigator} from './CartScreenNavigator';
import {AccountNavigator} from './AccountNavigator';
import {AppRoute} from './AppRoutes';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthNavigator} from './AuthNavigator';
const BottomTab = createBottomTabNavigator();
const Stack = createStackNavigator();

export const TabBarNavigator = () => {
  const [isUserLoggedIn, setIsUserLoggedIn] = React.useState(false);
  const [userData, setUserData] = React.useState();

  const isFocused = useIsFocused();
  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@storage_Key');
      if (jsonValue === null) {
        setIsUserLoggedIn(false);
      } else {
        setIsUserLoggedIn(true);
      }
    } catch (e) {
      console.log('error');
    }
  };

  React.useEffect(() => {
    getData();
  });

  return (
    <BottomTab.Navigator
      initialRouteName={AppRoute.HOME}
      screenOptions={{
        tabBarStyle: {
          borderTopColor: 'rgba(0,0,0,0.8)',
        },
      }}>
      <BottomTab.Screen
        name={AppRoute.HOME}
        component={HomePageNavigator}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: () => <Icon name="home" size={15} />,
          headerShown: false,
        }}
      />
      <BottomTab.Screen
        name={AppRoute.CART}
        component={CartScreenNavigator}
        options={{
          tabBarLabel: 'Cart',
          tabBarIcon: () => <Icon name="shopping-cart" size={15} />,
          headerShown: false,
        }}
      />
      <BottomTab.Screen
        name={AppRoute.MY_ACCOUNT}
        component={isUserLoggedIn ? AccountNavigator : AuthNavigator}
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: () => <Icon name="user-circle" size={15} />,
          headerShown: false,
        }}
      />
    </BottomTab.Navigator>
  );
};

export const AppNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="App"
        component={TabBarNavigator}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
