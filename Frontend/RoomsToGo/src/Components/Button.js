import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

const Button = props => {
  const {
    backgroundColor,
    label,
    onPress,
    width,
    height,
    disabled,
    customStyle,
    labelStyle,
  } = props;

  return (
    <TouchableOpacity
      style={[
        styles.container,
        customStyle && customStyle,
        backgroundColor && {backgroundColor: backgroundColor},
        width && {width: width},
        height && {height: height},
      ]}
      onPress={onPress}
      disabled={disabled}>
      <Text style={[styles.label, labelStyle && labelStyle]}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 15,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    width: 200,
    backgroundColor: '#9EB7B8',
  },
  label: {
    alignItems: 'center',
    fontSize: 17,
    color: 'white',
  },
});

export default Button;
