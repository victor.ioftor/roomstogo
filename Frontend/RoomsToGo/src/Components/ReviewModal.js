import React, {useState} from 'react';
import {View, TextInput, Text, StyleSheet, Modal, Alert} from 'react-native';
import Button from './Button';
import axios from 'axios';

const ReviewModal = props => {
  const {visible, onClose, productId, userId, userName} = props;
  const [rating, setRating] = useState(0);
  const [message, setMessage] = useState('');

  const giveReview = async () => {
    if (productId.length != 0 && userId.length != 0) {
      try {
        const res = await axios
          .post('/giveReview', {
            productId: productId,
            userId: userId,
            firstName: userName,
            rating: rating,
            comment: message,
          })
          .catch(function (error) {
            if (error.response) {
              console.log(error.response.data.text);
            } else if (error.request) {
              console.log(error.request);
            } else {
              console.log('Error', error.message);
            }
          });
        if (res) {
          console.log('res data', res);
        }
      } catch {
        Alert.error('Something went wrong');
      }
    } else {
      Alert.error('Please fill all the fields');
    }
  };

  const handleReviewSubmit = () => {
    giveReview();
    //onClose();
  };

  return (
    <Modal visible={visible} transparent={true}>
      <View style={styles.reviewModalContainer}>
        <View style={styles.reviewModalContent}>
          <View style={{alignItems: 'center'}}>
            <Text style={styles.reviewModalTitle}>Please add a Review</Text>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.textLabel}>
              Please insert a rating ranging from 1 to 5
            </Text>
            <TextInput
              style={styles.reviewInput}
              placeholder="Rating"
              keyboardType="numeric"
              onChangeText={rating => setRating(rating)}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.textLabel}>Please insert a message</Text>
            <TextInput
              style={styles.reviewInput}
              placeholder="Message"
              multiline={true}
              onChangeText={message => setMessage(message)}
            />
          </View>
          <View style={styles.submitButton}>
            <Button
              customStyle={styles.submitButton}
              onPress={handleReviewSubmit}
              label={'Submit'}
              backgroundColor={'#284B63'}
              width={150}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  reviewModalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    padding: 16,
  },
  reviewModalContent: {
    backgroundColor: 'white',
    borderRadius: 8,
    width: '100%',
    maxWidth: 500,
    padding: 16,
  },
  reviewModalTitle: {
    fontSize: 18,
    fontWeight: '600',
    marginBottom: 16,
  },
  inputContainer: {
    justifyContent: 'space-around',
  },
  textLabel: {
    marginBottom: 10,
  },
  reviewInput: {
    width: '100%',
    marginBottom: 16,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    padding: 8,
  },
  submitButton: {
    borderRadius: 8,
    paddingVertical: 10,
    alignItems: 'center',
  },
  submitButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default ReviewModal;
