import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import axios from 'axios';
const ProductCard = props => {
  const {
    productName,
    price,
    image,
    productId,
    cartId,
    showAddToCart,
    navigation,
  } = props;

  const addToCart = async () => {
    try {
      const res = await axios
        .post('/addToCart', {
          productId,
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
    } catch {
      console.log('Cannot add to cart');
    }
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() =>
        navigation.navigate('ProductDetailsScreen', {
          productId,
          productName,
          price,
          image,
          cartId,
        })
      }>
      <Image source={{uri: image}} style={styles.logo} />
      <View style={styles.productTitle}>
        <Text style={styles.title}>{productName}</Text>
      </View>
      <View style={styles.priceContainer}>
        <Text style={styles.price}>{price}$</Text>
      </View>
      {showAddToCart === true && (
        <View>
          <TouchableOpacity
            style={styles.cartButton}
            onPress={() => addToCart()}>
            <Text style={styles.cartText}>Add to Cart</Text>
          </TouchableOpacity>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 5,
  },
  logo: {
    resizeMode: 'contain',
    height: 120,
    width: 120,
    borderRadius: 10,
  },
  productTitle: {
    marginTop: 10,
    marginBottom: 10,
  },
  priceContainer: {
    paddingTop: 5,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  price: {
    fontSize: 13,
  },
  cartButton: {
    backgroundColor: '#153243',
    marginTop: 8,
    marginBottom: 5,
    width: 100,
    borderRadius: 50,
    height: 35,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartText: {
    alignItems: 'center',
    fontSize: 14,
    color: 'white',
  },
});

export default ProductCard;
