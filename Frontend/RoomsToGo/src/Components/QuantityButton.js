import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';

const QuantityButton = ({quantity, cartId, productId, onChangeQuantity}) => {
  const [quantityButton, setQuantityButton] = useState(quantity);

  const addToCart = async () => {
    try {
      const res = await axios
        .post('/addToCart', {
          productId,
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
    } catch {
      console.log('Cannot add to cart');
    }
  };

  const deleteFromCart = async () => {
    try {
      const res = await axios
        .post('/deleteProductFromCart', {
          productId,
          cartId,
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        });
    } catch {
      console.log('Cannot add to cart');
    }
  };

  useEffect(() => {
    setQuantityButton(quantity);
  }, [quantity]);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        disabled={quantityButton < 1}
        onPress={() => {
          deleteFromCart();
          onChangeQuantity(quantityButton - 1);
          setQuantityButton(quantityButton - 1);
        }}>
        <MaterialCommunityIcons
          name="minus"
          size={15}
          color="black"
          style={styles.minusIcon}
        />
      </TouchableOpacity>
      <Text style={styles.textQuantity}>{quantityButton}</Text>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          addToCart();
          onChangeQuantity(quantityButton + 1);
          setQuantityButton(quantityButton + 1);
        }}>
        <MaterialCommunityIcons
          name="plus"
          size={15}
          color="black"
          style={styles.plusIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginLeft: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#6c757d',
    borderRadius: 8,
    width: 100,
    height: 30,
  },
  textQuantity: {
    fontSize: 16,
  },
  minusIcon: {
    marginLeft: 5,
  },
  plusIcon: {
    marginRight: 5,
  },
});

export default QuantityButton;
