import React from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import ProductCard from './ProductCard';

const ProductGrid = props => {
  const {products, showAddToCart, navigation} = props;
  const _renderRow = ({item, index}) => {
    return (
      <View key={index} style={styles.gridRow}>
        {item.map(product => (
          <ProductCard
            productName={product.productName}
            price={product.price}
            image={product.image}
            productId={product.id}
            cartId={props?.cartId}
            showAddToCart={showAddToCart}
            navigation={navigation}
          />
        ))}
      </View>
    );
  };

  let rows = [],
    rowItems,
    i,
    j;

  for (i = 0, j = products?.length; i < j; i += 2) {
    rowItems = products.slice(i, i + 2);
    rows.push(rowItems);
  }

  return (
    <>
      <FlatList
        keyboardShouldPersistTaps="handled"
        renderItem={_renderRow}
        data={rows}
        style={styles.containerFlatList}
      />
    </>
  );
};

const styles = StyleSheet.create({
  containerFlatList: {
    flex: 1,
    paddingHorizontal: 1,
  },
  gridRow: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ProductGrid;
