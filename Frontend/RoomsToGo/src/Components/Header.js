import {React, useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MyAccountScreen from '../Screens/MyAccountScreen';

const Header = props => {
  const {navigation, title} = props;
  const [goBackTrigger, setGoBackTrigger] = useState(true);

  useEffect(() => {
    if (title === 'Home') {
      setGoBackTrigger(false);
    }
    if (title === 'Cart') {
      setGoBackTrigger(false);
    }
    if (title === 'Log In') {
      setGoBackTrigger(false);
    }
    if (title === 'My Account') {
      setGoBackTrigger(false);
    }
    if (title === 'Edit Details') {
      setGoBackTrigger(false);
    }
  }, [title]);

  return (
    <SafeAreaView style={styles.backgroundColor}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          {goBackTrigger && (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              testID="headerBackButton"
              style={styles.icon}>
              <Icon name="arrow-left" size={23} />
            </TouchableOpacity>
          )}
          <View style={styles.titleDisplay}>
            <Text style={styles.titleScreen}>{title}</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    elevation: 2,
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  icon: {
    marginHorizontal: 3,
    position: 'absolute',
    left: 2,
  },
  titleScreen: {
    color: 'black',
    fontSize: 16,
    fontWeight: '700',
  },
  titleDisplay: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundColor: {
    color: '#f1faee',
  },
});

export default Header;
