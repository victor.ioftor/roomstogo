import React from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import OrderCard from './OrderCard';

const OrdersGrid = ({orders}) => {
  const _renderRow = ({item, index}) => {
    return (
      <View key={index} style={styles.gridRow}>
        {item?.map(order => (
          <OrderCard
            firstName={order.firstName}
            lastName={order.lastName}
            address={order.address}
            phoneNumber={order.phoneNumber}
            totalSum={order.totalSum}
          />
        ))}
      </View>
    );
  };

  let rows = [],
    rowItems,
    i,
    j;
  for (i = 0, j = orders?.length; i <= j; i += 1) {
    rowItems = orders?.slice(i, i + 1);
    rows.push(rowItems);
  }

  return (
    <>
      <FlatList
        keyboardShouldPersistTaps="handled"
        renderItem={_renderRow}
        keyExtractor={(item, index) => `row-${index}`}
        data={rows}
        style={styles.containerFlatList}
      />
    </>
  );
};

const styles = StyleSheet.create({
  containerFlatList: {
    flex: 1,
    paddingHorizontal: 1,
    backgroundColor: 'white',
  },
  gridRow: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default OrdersGrid;
