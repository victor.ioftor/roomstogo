import React from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import CartProductCard from './CartProductCard';

const CartGrid = ({products, cartId, onChangeQuantity}) => {
  const _renderRow = ({item, index}) => {
    return (
      <View key={index} style={styles.gridRow}>
        {item.map(product => (
          <CartProductCard
            productName={product.productName}
            price={product.price}
            image={product.image}
            quantity={product.quantity}
            cartId={cartId}
            productId={product.id}
            onChangeQuantity={onChangeQuantity}
          />
        ))}
      </View>
    );
  };

  let rows = [],
    rowItems,
    i,
    j;
  for (i = 0, j = products.length; i <= j; i += 1) {
    rowItems = products.slice(i, i + 1);
    rows.push(rowItems);
  }

  return (
    <>
      <FlatList
        keyboardShouldPersistTaps="handled"
        renderItem={_renderRow}
        keyExtractor={(item, index) => `row-${index}`}
        data={rows}
        style={styles.containerFlatList}
      />
    </>
  );
};

const styles = StyleSheet.create({
  containerFlatList: {
    flex: 1,
    paddingHorizontal: 1,
  },
  gridRow: {
    flex: 1,
    paddingTop: 10,
    marginTop: 5,
    paddingBottom: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 5,
  },
});

export default CartGrid;
