import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import QuantityButton from './QuantityButton';

const CartProductCard = ({
  productName,
  price,
  image,
  quantity,
  cartId,
  productId,
  onChangeQuantity,
}) => {
  return (
    <View style={styles.container}>
      <Image source={{uri: image}} style={styles.image} />
      <View style={styles.detailsContainer}>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {productName}
          </Text>
        </View>
        <View style={styles.priceContainer}>
          <Text style={styles.price}>{price}$</Text>
        </View>
        <QuantityButton
          quantity={quantity}
          cartId={cartId}
          productId={productId}
          onChangeQuantity={onChangeQuantity}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginLeft: 14,
    borderRadius: 5,
    flexDirection: 'row',
    height: 180,
    width: 360,
  },
  image: {
    resizeMode: 'contain',
    height: 135,
    width: 135,
    borderRadius: 10,
    marginLeft: 10,
    marginTop: 22,
  },
  detailsContainer: {
    marginTop: 28,
    alignContent: 'center',
    flexDirection: 'column',
    marginLeft: 10,
  },
  productTitle: {
    marginTop: 10,
    marginBottom: 10,
    ///marginLeft: 25,
    width: 180,
  },
  quantityContainer: {
    //marginLeft: 50,
  },
  priceContainer: {
    paddingTop: 10,
    //marginLeft: 40,
    paddingBottom: 10,
  },
  title: {
    fontSize: 17,
  },
  price: {
    fontSize: 15,
    //paddingLeft: 35,
  },
});

export default CartProductCard;
