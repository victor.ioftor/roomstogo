import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const OrderCard = props => {
  const {firstName, lastName, address, phoneNumber, totalSum} = props;

  return (
    <View style={styles.container}>
      <View style={styles.detailsContainer}>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {firstName}
          </Text>
        </View>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {lastName}
          </Text>
        </View>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {address}
          </Text>
        </View>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {phoneNumber}
          </Text>
        </View>
        <View style={styles.productTitle}>
          <Text style={styles.title} numberOfLines={2}>
            {totalSum}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E7EAEA',
    marginLeft: 14,
    borderRadius: 5,
    flexDirection: 'row',
    height: 180,
    width: 360,
  },
  detailsContainer: {
    marginTop: 20,
  },
  productTitle: {
    marginBottom: 10,
    marginLeft: 25,
    width: 180,
  },
  title: {
    fontSize: 17,
  },
});

export default OrderCard;
